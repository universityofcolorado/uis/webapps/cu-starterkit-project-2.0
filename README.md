# Composer-enabled Drupal template

[![Actively Maintained](https://img.shields.io/badge/Pantheon-Actively_Maintained-yellow?logo=pantheon&color=FFDC28)](https://pantheon.io/docs/oss-support-levels#actively-maintained)

This is Pantheon's recommended starting point for forking new [Drupal](https://www.drupal.org/) upstreams
that work with the Platform's Integrated Composer build process. It is also the
Platform's standard Drupal 9 upstream.

Unlike with earlier Pantheon upstreams, files such as Drupal Core that you are
unlikely to adjust while building sites are not in the main branch of the 
repository. Instead, they are referenced as dependencies that are installed by
Composer.

For more information and detailed installation guides, please visit the
Integrated Composer Pantheon documentation: https://pantheon.io/docs/integrated-composer

## Contributing

Contributions are welcome in the form of GitHub pull requests. However, the
`pantheon-upstreams/drupal-composer-managed` repository is a mirror that does not
directly accept pull requests.

Instead, to propose a change, please fork [pantheon-systems/drupal-composer-managed](https://github.com/pantheon-systems/drupal-composer-managed)
and submit a PR to that repository.


----


## Project Initialization

When first creating a Pantheon Drupal Project you will need to hook it into the Pantheon site code. Use the following steps to make the integration.

Once setup, **ALL commits and merges need to move through Gitlab**. Committing directly to Pantheon will cause merge conflicts.

## Create the projects

First up, create a new GitLab project with the [Pantheon Project Template](https://gitlab.com/universityofcolorado/uis/webapps/templates-internal/pantheon-project-template).

Now, create a new site on Pantheon with the appropriate Custom Upstream.

*You might be tempted to make some changes, such as adding or removing modules, but please refrain. We haven't connected the site to GitLab yet and want to make sure all code changes, e.g. adding or removing modules, go through GitLab.*

Now go back to the Pantheon site dashboard and and ensure the development mode is set to Git.

*REFERENCE: [Giltab to Pantheon](https://about.gitlab.com/blog/2019/03/26/connecting-gitlab-and-pantheon-streamline-wordpress-drupal-workflows/)*

## Initial commit to GitLab

Next, we need to get the starting code from the Pantheon site over to GitLab. In order to do this, we will clone the code from the Pantheon site *Git* repository locally, then push it to the GitLab repository.

To make this easier, and more secure, [add an SSH key to Pantheon](https://pantheon.io/docs/ssh-keys/) to avoid entering your password when cloning Pantheon Git repository. While you're at it, [add an SSH key to GitLab](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account) as well.

To do this, clone the Pantheon site locally by copying the command in the *Clone with Git* drop-down field from the Pantheon site dashboard.

**Notice** - Pantheon uses `master` for its default branch where our Gitlab uses `main`. In order to make the transiton between default branches choose one of the options below. Each option has it's own CI/CD jobs in order to work with Pantheon properly.

### [Option 1 - Integrate `main` to `master` *(**NOT** currently supported in Pantheon)*](/private/ci-cd/ci-cd-master-to-main.md)

### [Option 2 - Integrate `master` to `master` *(currently supported in Pantheon)*](/private/ci-cd/ci-cd-master-to-master.md)

## Setup Pantheon Mirroring

TODO - document setting up Mirroring repositories for Gitlab and Pantheon



---



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
