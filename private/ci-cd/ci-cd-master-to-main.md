# Pantheon integration with Gitlab


## Connect Pantheon/Gitlab repositories

Use the following steps to integrate Pantheon default `master` branch with Gitlab default `main` branch. You need to be working in your local **Pantheon Site** repo you created earlier.

>>>
### Step 1 - Move the 'master' branch to 'main'

Run the following command which creates a branch called `main` using the history from `master`. Using the argument `-m` will transfer all of the commit history on the `master` branch to your new `main` branch so nothing gets lost.

```
git branch -m master main
```
*[Reference](https://stevenmortimer.com/5-steps-to-change-github-default-branch-from-master-to-main/)*

### Step 2 - Remove overlapping files

Pantheon creates some overlapping files when using a *Custom Upstream*. Therefore we need to remove these files before merging repos. 

**Delete** the following files in the `root` directory: `.gitlab-ci.yml, README.md, CHANGELOG.md`.

We then need to commit the changes to the new `main` branch.

```
git add .gitlab-ci.yml README.md CHANGELOG.md
```

then run

```
git commit -m "Removing overlapping repo files."
```

### Step 3 - Push ‘main’ to remote repo

Remember that git is version control software on your local machine and Pantheon is the remote server that stores your code. For this reason, you’ll have to push your new `main` branch up to Pantheon and tell the local branch to start tracking the remote branch with the same name.

```
git push -u origin main
```

### Step 4 - Point HEAD to ‘main’ branch

At this stage if `master` was your default branch you cannot remove it without first changing `HEAD`, the pointer to the current branch reference. This following command will point it to `main`.

```
git symbolic-ref refs/remotes/origin/HEAD refs/remotes/origin/main
```

### Step 5 - Change git remote

Next, we want to change the `git remote origin` to point to GitLab, instead of Pantheon. This can be done with the `git remote` command.

Head over to your GitLab project and grab the repository URL, which can be found in the *Clone* drop-down of the project details screen. Be sure to use the *Clone with SSH* variant of the GitLab repository URL, since we set up an SSH key earlier.

```
git remote set-url origin [GitLab repository URL]
```
*Replacing `[GitLab repository URL]` with your actual GitLab repository URL.*

### Step 6 - Remove 'master' remote branch

Your local repo still has the `master` remote branch, therefore we need to `prune` it.

```
git fetch --prune origin
```

### Step 7 - Allow Commit Authors outside CU

Before we can merge the repos we need to merge historical commits from non-CU users. CU Gitlab protects the `main` branch and requires `push` to have a valid CU email address for all commits. Therefore, we need to change the project setting to allow this initial commit. 

You will need go into your Gitlab project **Settings > Repository** dashboard to make the changes. First, under **Push Rules**, un-check `Reject unverified users` and `Check whether the commit author is a GitLab user` then save the **Push Rules**. Under **Protected branches**, select to **Unprotect** the `main` branch. *You will re-protect the `main` branch after our initial push.*

### Step 8 - Merge repos

Now we can merge the repos and their histories.

```
git pull origin main --allow-unrelated-histories
```

### Step 9 - Send code to Pantheon

Finally, send the code from the Pantheon site to GitLab.

```
git push origin main --force
```
*The –force flag is only needed as part of this one-time step. Subsequent `git push` commands to GitLab won't need it.*

You'll want to check that all your commits have been pushed.

```
git status
```

If there are still some un-commited files re-run `git commit -m "Final merge commit."`, followed by a `git push`.

### Step 10 - Clean Up

#### Pantheon

Go back to your Pantheon site dashboard for **Multidev** and go into the **Git Branches** section. You should see your `main` branch from earlier. **Delete** that branch to avoid further conflicts between repos.

#### Reprotect Gitlab 'main' branch

Now we can re-protect your main branch in Gitlab.

Go into your Gitlab project **Settings > Repository** dashboard to make the change. Then, under **Protected branches**, select the `main` branch to protect. CU's default settings are to only allow **Maintainers** to *merge/push* and not allow **Force Push** or **Require approval**.

> If you prefer, you can also go and turn on your project **Push Rules**: `Reject unverified users`, `Check whether the commit author is a GitLab user`. Save your changes. *Optionally, you can add `@(cu|uccs|colorado|ucdenver)\.edu$` to **Commit author's email** value to enforce CU emails addresses for your authors.*

### Step 11 - Final sync

We need to do one last `push` from the local repo to Pantheon to ensure `master` branch and `refs` are in sync. In your local **Pantheon Project** repository, do the following.

#### Get latest from Gitlab

```
git checkout main
```
```
git fetch
```
```
git pull
```

#### Create Pantheon Remote

```
git remote add pantheon [Pantheon repository URL]
```
*Replacing `[Pantheon repository URL]` with your actual Pantheon repository URL.*

#### Push to Pantheon

```
git push pantheon HEAD:master --force
```

Go to your Pantheon **DEV** dashboard to verify your `push` is showing up in the code view. You should see your *Updating overlapping repo files.* commit from earlier.


Your Pantheon site should now be integrated into your CU Gitlab project. **ALL commits and merges need to move through Gitlab now!**
>>>


## Create Pipeline for auto-deply to Pantheon

We will not go through and setup a pipeline to auto-deploy your code to Pantheon


### Set up credentials and variables for Pipelines

Remember how we added an SSH key locally to authorize with Pantheon and GitLab? Well, an SSH token can also be used to authorize GitLab and Pantheon.

We have a Service User Account setup between Pantheon and Gitlab to perform CI/CD actions. Within the Gitlab project settings you can see the inherited CI/CD Environment Variables: **GITLAB_USER_EMAIL**, **SSH_PRIVATE_KEY**, **PANTHEON_MACHINE_TOKEN**.

We will need to set some project environment variables. The first one should be named **PANTHEON_SITE**, and the value will be the *machine name* of your Pantheon site.

You can get the machine name from the end of the `Clone with Git` command. Since you already cloned the site locally, it will be the directory name of your local repository.

The next GitLab CI/CD environment variable to set is **PANTHEON_GIT_URL**, which will be the Git repository URL of the Pantheon site that we used earlier.

*Enter just the SSH repository URL, leaving off all `git` commands and the site machine name at the end.*


### Create the deployment job

What we will be doing with GitLab CI/CD initially is very similar to what we did with Git repositories earlier. This time though, we will add the Pantheon repository as a second Git remote and then push the code from GitLab to Pantheon.

Within the `.gitlab-ci.yaml` we have some pre-configured code as a starter point. You can un-comment these sections to enable them for the project. Use the job sections under the **CI/CD jobs for masin -> master default branches** comment for this.

To Start, we will focus on enabling the deploy from Gitlab to Pantheon. 

>>>
Create a new local `git` branch.

```
git checkout -b deploy-dev
```

Edit `.gitlab-ci.yml` by un-commenting the `before_script` and `deploy-dev` sections.

```
before_script:
  # See https://docs.gitlab.com/ee/ci/ssh_keys/README.html
  - apk update && apk add git
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
  - mkdir -p $HOME/.ssh && echo "StrictHostKeyChecking no" >> "$HOME/.ssh/config"
  - git config --global user.email "$GITLAB_USER_EMAIL"
  - git config --global user.name "Gitlab CI/CD"
  # un-comment if needed
#  - git config --global http.postBuffer 524288000
```

```
deploy-dev:
  stage: deploy
  environment:
    name: dev
    url: https://dev-$PANTHEON_SITE.pantheonsite.io/
  script:
      # Add the Pantheon git repository as an additional remote
    - git remote add pantheon $PANTHEON_GIT_URL
    - git push pantheon HEAD:master --force
  only:
    - main
```

*You should also comment out the test `deploy-job` section, as well.*

Finally, save, commit, and push the `.gitlab-ci.yml` file to send it to GitLab.

```
git add .gitlab-ci.yml
```
```
git commit -m "Update .gitlab-ci.yml file with deploy-dev job"
```
```
git push --set-upstream origin deploy-dev
```

Then complete this with a merge request to the `main` branch.
>>>

**SSH_PRIVATE_KEY**, **PANTHEON_SITE**, and **PANTHEON_GIT_URL** should all look familiar - they are the environment variables we set up earlier. Having environment variables will allow us to re-use the values multiple times in our `.gitlab-ci.yml` file, while having one place to update them, should they change in the future.


## Verify the deployment

If everything was done correctly, the `deploy-dev` job will run on **GitLab CI/CD** succeed and send the `.gitlab-ci.yml` commit to Pantheon.


## Optional CI/CD

To use the other pre-configured CI/CD features, follow the instructions below.

### Sending merge request branches to Pantheon

This next section makes use of the Pantheon feature, [Multidev](https://pantheon.io/docs/guides/multidev), which allows you to create additional Pantheon environments on demand associated with Git branches.

This section is entirely optional as [Multidev access is restricted](https://pantheon.io/docs/guides/multidev/multidev-faq). However, if you do have multidev access, having GitLab merge requests automatically create multidev environments on Pantheon is a huge workflow improvement.

Start by making a new Git branch locally with `git checkout -b multidev-support`. Now, let's edit `.gitlab-ci.yml` again.

We are using the merge request number in the Pantheon environment name. For example, the first merge request would be `mr-1`, the second would be `mr-2`, and so on.

Since the merge request changes, we need to define these Pantheon branch names dynamically. GitLab makes this easy by providing [predefined environment](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) variables.

We can use `CI_MERGE_REQUEST_IID`, which provides the merge request number. A `deploy-multidev` job is commented out at the end of our `.gitlab-ci.yml` file. This job is not automated and merely pushes your branch to Pantheon, making it available to manually create a multidev environment. 

*There is an automated job that utilizes [Terminus](https://pantheon.io/docs/terminus) to create the multidev environment. That is covered below but it's good to start with testing the manual process first.*

This should look very similar to our `deploy-dev` job, only pushing a branch to Pantheon instead of `main`.

After you un-comment the `deploy-multidev` section and commit the updated `.gitlab-ci.yml` file, push this new branch to GitLab with `git push -u origin multidev-support`.

Next, create a new merge request from our `multidev-support` branch by following the *Create merge request* prompt.

After creating the merge request, look for the CI/CD job `deploy-multidev` to run.

You should see a new branch was sent to Pantheon. However, when we go to the multidev section of the site dashboard on Pantheon there isn't a new multidev environment. Instead, look at the *Git Branches* section.

The `mr-1` branch did make it to Pantheon after all. Go ahead and create an environment from the `mr-1` branch.

Once the multidev environment has been created, head back to GitLab and look at the *Operations > Environments* section. You will notice entries for `dev` and `mr-1`.

This is because we added an `environment` entry with `name` and `url` to our CI/CD jobs. If you click on the open environment icon, you will be taken to the URL for the multidev on Pantheon.

### Automating multidev creation

Pantheon has a command line tool, [Terminus](https://pantheon.io/docs/terminus), that allows you to interact with the platform in an automated fashion. Terminus will allow us to provision our multidev environments from the command line.

We will need a new merge request to test this, so create a new branch with `git checkout -b auto-multidev-creation`.

In order to use Terminus in GitLab CI/CD jobs we will need a machine token to authenticate with Terminus and a container image with Terminus available. A global GitLab environment variable named **PANTHEON_MACHINE_TOKEN** has already been created and made available.

>>>
#### Building a Dockerfile with Terminus

TODO - Docker [container](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html).

The `deploy-multidev_auto` job exists in the `.gitlab-ci.yml`, which uses a dedicated file in `private/ci-cd/multidev-deploy.sh`. The script is in the `private` directory as [it is not web accessible](https://pantheon.io/docs/guides/secure-development/private-paths) on Pantheon.

Un-comment the `deploy-multidev_auto` section and comment out the previous `deploy-multidev` section. Save, commit, and push `.gitlab-ci.yml`. Now, head back to GitLab and wait for the CI/CD job to finish. The multidev creation takes a few minutes, so be patient.

When it is finished, go check out the multidev list on Pantheon. The `mr-2` multidev should be there.

*More CI/CD options through [Terminus Build Tools](https://github.com/pantheon-systems/terminus-build-tools-plugin/).*
>>>
