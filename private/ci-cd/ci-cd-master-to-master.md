# Pantheon integration with Gitlab


Use the following steps to integrate Pantheon default `master` branch with Gitlab new default `master` branch. To make things easier you should clone your **Gitlab** project to temporary local directory. Then later we'll switch to your local **Pantheon Site** repo to finish the integration.


## Change Gitlab default branch

Work in your temporary local **Gitlab Project** repository.

*[Reference](https://stevenmortimer.com/5-steps-to-change-github-default-branch-from-master-to-main/)*

>>>
### Step 1 - Move the 'main' branch to 'master'

Run the following command which creates a branch called `master` using the history from `main`. Using the argument `-m` will transfer all of the commit history on the `main` branch to your new `master` branch so nothing gets lost.

```
git branch -m main master
```

### Step 2 - Push 'master' to remote repo

Remember that git is version control software on your local machine and Gitlab is the remote server that stores your code. For this reason, you’ll have to push your new `master` branch up to Gitlab and tell the local branch to start tracking the remote branch with the same name.

```
git push -u origin master
```

### Step 3 - Point HEAD to ‘master’ branch

At this stage if `main` was your default branch you cannot remove it without first changing `HEAD`, the pointer to the current branch reference. This following command will point it to `master`.

```
git symbolic-ref refs/remotes/origin/HEAD refs/remotes/origin/master
```

At this point you can verify your local `HEAD` is pointing to `master`

```
git branch -a
```

### Step 4 - Change default branch to 'master' on Gitlab project

At this point you’ve succesfully transitioned everything to the `master` branch, but you can’t delete the `main` branch without changing the default branch in Gitlab to something other than `main`. This is the only step that requires you to leave the Terminal and navigate in your browser to your Gitlab repository. From there, click *Settings -> Repository* on the left rail and change the default branch to `master`.

Next, you will want to change which branches are protected. In the same *Gitlab Repository Settings dashboard*, unprotect the `main` branch. We'll protect the `master` branch in later steps.

### Step 5 - Delete 'main' branch on the remote repo

Now that the local `main` is gone and there’s nothing pointing to it on Gitlab you can delete it with the following command:

```
git push origin --delete main
```

That’s it! You’re done! You should no longer see `main` or `remotes/origin/main` locally or on your Gitlab repository. If you want to check, you can always run the following:

```
git branch -a
```

You can now remove the *temporary* Gitlab project repository!
>>>


## Connect Pantheon to Gitlab

Work in your local **Pantheon Project** repository.

>>>
### Step 1 - Change git remote

First, we want to change the `git remote origin` to point to GitLab, instead of Pantheon. This can be done with the `git remote` command.

Head over to your GitLab project and grab the repository URL, which can be found in the *Clone* drop-down of the project details screen. Be sure to use the *Clone with SSH* variant of the GitLab repository URL, since we set up an SSH key earlier.

```
git remote set-url origin [GitLab repository URL]
```
*Replacing `[GitLab repository URL]` with your actual GitLab repository URL.*

### Step 2 - Allow Commit Authors outside CU

Before we can merge the repos we need to merge historical commits from non-CU users. CU Gitlab protects the `master` branch and requires `push` to have a valid CU email address for all commits. Therefore, we need to change the project settings to allow this initial commit. 

You will need go into your Gitlab project **Settings > Repository** dashboard to make the changes. First, under **Push Rules**, un-check `Reject unverified users` and `Check whether the commit author is a GitLab user` then save the **Push Rules**. Under **Protected branches**, select to **Unprotect** the `master` branch. *You will re-protect the `master` branch after our initial push.*

### Step 3 - Merge repos

Now we can merge the repos and their histories.

```
git pull origin master --allow-unrelated-histories
```

### Step 4 - Update overlapping files

You should have gotten some **Merge Conflicts** for `.gitlab-ci.yml` and `README.md`. Pantheon creates some overlapping files when using a *Custom Upstream*. Therefore we need to update these files. 

**Accept incoming changes** for the following files in the `root` directory: `.gitlab-ci.yml, README.md`.

*You may want to delete `CHANGELOG.md` as well, since it is the Custom Upstream change log.*

We then need to commit the changes to the new `master` branch.

```
git add .gitlab-ci.yml README.md
```

then run

```
git commit -m "Updating overlapping repo files."
```

### Step 5 - Send code to Gitlab

Finally, send the code from the Pantheon site to GitLab.

```
git push origin master --force
```
*The –force flag is only needed as part of this one-time step. Subsequent `git push` commands to GitLab won't need it.*

You'll want to check that all your commits have been pushed.

```
git status
```

If there are still some un-commited files re-run `git commit -m "Final merge commit."`, followed by a `git push`.

### Step 10 - Clean Up

#### Reprotect Gitlab 'master' branch

Now we can re-protect your `master` branch in Gitlab.

Go into your Gitlab project **Settings > Repository** dashboard to make the change. Then, under **Protected branches**, select the `master` branch to protect. CU's default settings are to only allow **Maintainers** to *merge/push* and not allow **Force Push** or **Require approval**.

> If you are not going to have non-CU contributors, you can also go and turn on your project **Push Rules**: `Reject unverified users`, `Check whether the commit author is a GitLab user`. Save your changes. *Optionally, you can add `@(cu|uccs|colorado|ucdenver)\.edu$` to **Commit author's email** value to enforce CU emails addresses for your authors.*

### Step 11 - Final sync

We need to do one last `push` from the local repo to Pantheon to ensure `master` branch and `refs` are in sync.

#### Get latest from Gitlab

```
git checkout master
```
```
git fetch
```
```
git pull
```

#### Create Pantheon Remote

```
git remote add pantheon [Pantheon repository URL]
```
*Replacing `[Pantheon repository URL]` with your actual Pantheon repository URL.*

#### Push to Pantheon

```
git push pantheon HEAD:master --force
```

Go to your Pantheon **DEV** dashboard to verify your `push` is showing up in the code view. You should see your *Updating overlapping repo files.* commit from earlier.

Your Pantheon site should now be integrated into your CU Gitlab project. **ALL commits and merges need to move through Gitlab now!**
>>>


## Create Pipeline for auto-deply to Pantheon

We will not go through and setup a pipeline to auto-deploy your code to Pantheon


### Set up credentials and variables for Pipelines

Remember how we added an SSH key locally to authorize with Pantheon and GitLab? Well, an SSH token can also be used to authorize GitLab and Pantheon.

We have a Service User Account setup between Pantheon and Gitlab to perform CI/CD actions. Within the Gitlab project settings you can see the inherited CI/CD Environment Variables: **GITLAB_USER_EMAIL**, **SSH_PRIVATE_KEY**, **PANTHEON_MACHINE_TOKEN**.

We will need to set some project [environment variables](https://docs.gitlab.com/ee/ci/variables/#gitlab-cicd-variables). The first one should be named **PANTHEON_SITE**, and the value will be the *machine name* of your Pantheon site. Only select the **Expand variable reference** flag.

You can get the machine name from the end of the `Clone with Git` command. Since you already cloned the site locally, it will be the directory name of your local repository.

The next GitLab CI/CD environment variable to set is **PANTHEON_GIT_URL**, which will be the Git repository URL of the Pantheon site that we used earlier. Only select the **Expand variable reference** flag.

*Enter just the SSH repository URL, leaving off all `git` commands and the site machine name at the end.*


### Create the deployment job

What we will be doing with GitLab CI/CD initially is very similar to what we did with Git repositories earlier. This time though, we will add the Pantheon repository as a second Git remote and then push the code from GitLab to Pantheon.

Within the `.gitlab-ci.yaml` we have some pre-configured code as a starter point. You can un-comment these sections to enable them for the project. Use the job sections under the **CI/CD jobs for master -> master default branches** comment for this.

To Start, we will focus on enabling the deploy from Gitlab to Pantheon. 

>>>
Create a new local `git` branch.

```
git checkout -b deploy-dev
```

Edit `.gitlab-ci.yml` by un-commenting the `before_script` and `deploy-dev` sections.

```
before_script:
  # See https://docs.gitlab.com/ee/ci/ssh_keys/README.html
  - apk update && apk add git
  - eval $(ssh-agent -s)
  - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
  - mkdir -p $HOME/.ssh && echo "StrictHostKeyChecking no" >> "$HOME/.ssh/config"
  - git config --global user.email "$GITLAB_USER_EMAIL"
  - git config --global user.name "Gitlab CI/CD"
  # un-comment if needed
#  - git config --global http.postBuffer 524288000
```

```
deploy-dev:
  stage: deploy
  environment:
    name: dev
    url: https://dev-$PANTHEON_SITE.pantheonsite.io/
  script:
    - git remote set-url origin $PANTHEON_GIT_URL
    - git push origin HEAD:master --force
  only:
    - master
```

*You should also comment out the test `deploy-job` section, as well.*

**SSH_PRIVATE_KEY**, **PANTHEON_SITE**, and **PANTHEON_GIT_URL** should all look familiar - they are the environment variables we set up earlier. Having environment variables will allow us to re-use the values multiple times in our `.gitlab-ci.yml` file, while having one place to update them, should they change in the future.

Finally, save, commit, and push the `.gitlab-ci.yml` file to send it to GitLab.

```
git add .gitlab-ci.yml
```
```
git commit -m "Update .gitlab-ci.yml file with deploy-dev job"
```
```
git push --set-upstream origin deploy-dev
```

Then complete this with a merge request to the `master` branch.
>>>

### Verify the deployment

If everything was done correctly, the `deploy-dev` job will run on **GitLab CI/CD** succeed and send the `.gitlab-ci.yml` commit to Pantheon. Go to your Pantheon **DEV** dashboard to verify your `merge` is showing up in the code view.



## Optional CI/CD - *in development*

TODO - Running into cache issue with Merge Request pipelins not finding commited branch

`error: pathspec 'multidev-support' did not match any file(s) known to git`

Could be related to runners and our lack of a license. Believe it may be related to [Pre-clone script](https://docs.gitlab.com/ee/ci/runners/saas/linux_saas_runner.html#pre-clone-script).

----

To use the other pre-configured CI/CD features, follow the instructions below.

### Sending merge request branches to Pantheon

This next section makes use of the Pantheon feature, [Multidev](https://pantheon.io/docs/guides/multidev), which allows you to create additional Pantheon environments on demand associated with Git branches.

This section is entirely optional as [Multidev access is restricted](https://pantheon.io/docs/guides/multidev/multidev-faq). However, if you do have multidev access, having GitLab merge requests automatically create multidev environments on Pantheon is a huge workflow improvement.

#### Manual Multidev creation

This process will deploy a `merge` branch to Pantheon where you can then create a Multidev instance through the dashboard.

>>>
### Getting Started

Start by getting the latest changes locally.

```
git checkout master
```
```
git fetch
```
```
git pull
```

Then create a new Git branch locally.

```
git checkout -b multidev-support
```

### Add job to CI/CD

Now, let's edit `.gitlab-ci.yml` again.

We are using the merge request number in the Pantheon environment name. For example, the first merge request would be `mr-1`, the second would be `mr-2`, and so on.

Since the merge request changes, we need to define these Pantheon branch names dynamically. GitLab makes this easy by providing [predefined environment variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html). We can use `CI_MERGE_REQUEST_IID`, which provides the merge request number.

A `deploy-multidev` job is commented out at the end of our `.gitlab-ci.yml` file, under the **CI/CD jobs for master -> master default branches**. Un-comment the job:

```
deploy-multidev:
  stage: deploy
  environment:
    name: multidev/mr-$CI_MERGE_REQUEST_IID
    url: https://mr-$CI_MERGE_REQUEST_IID-$PANTHEON_SITE.pantheonsite.io/
  script:
    # Checkout the merge request source branch
    - git checkout $CI_COMMIT_REF_NAME
    # Add the Pantheon git repository as an additional remote
    - git remote add pantheon $PANTHEON_GIT_URL
    # Push the merge request source branch to Pantheon
    - git push pantheon $CI_COMMIT_REF_NAME:mr-$CI_MERGE_REQUEST_IID --force
  only:
    - merge_requests
```

*There is an automated job that utilizes [Terminus](https://pantheon.io/docs/terminus) to create the multidev environment. That is covered below but it's good to start with testing the manual process first.*

This should look very similar to our `deploy-dev` job, only pushing a branch to Pantheon instead of `master`.

After you un-comment the `deploy-multidev` section and commit the updated `.gitlab-ci.yml` file, push this new branch to GitLab with `git push --set-upstream origin multidev-support`.

### Create Merge Request

Next, create a new merge request from our `multidev-support` branch by following the *Create merge request* prompt.

After creating the merge request, look for the CI/CD job `deploy-multidev` to run.

### Create Multidev from Merge Request

You should see a new branch was sent to Pantheon. However, when we go to the multidev section of the site dashboard on Pantheon there isn't a new multidev environment. Instead, look at the *Git Branches* section.

The `mr-2` branch did make it to Pantheon after all. Go ahead and create an environment from the `mr-2` branch.

Once the multidev environment has been created, head back to GitLab and look at the *Operations > Environments* section. You will notice entries for `dev` and `mr-2`.

This is because we added an `environment` entry with `name` and `url` to our CI/CD jobs. If you click on the open environment icon, you will be taken to the URL for the multidev on Pantheon.
>>>

### Automating multidev creation

Pantheon has a command line tool, [Terminus](https://pantheon.io/docs/terminus), that allows you to interact with the platform in an automated fashion. Terminus will allow us to provision our multidev environments from the command line.

We will need a new merge request to test this, so create a new branch with `git checkout -b auto-multidev-creation`.

In order to use Terminus in GitLab CI/CD jobs we will need a machine token to authenticate with Terminus and a container image with Terminus available. A global GitLab environment variable named **PANTHEON_MACHINE_TOKEN** has already been created and made available.

>>>
##### Building a Dockerfile with Terminus

TODO - Docker [container](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html).

The `deploy-multidev_auto` job exists in the `.gitlab-ci.yml`, which uses a dedicated file in `private/ci-cd/multidev-deploy.sh`. The script is in the `private` directory as [it is not web accessible](https://pantheon.io/docs/guides/secure-development/private-paths) on Pantheon.

Un-comment the `deploy-multidev_auto` section and comment out the previous `deploy-multidev` section. Save, commit, and push `.gitlab-ci.yml`. Now, head back to GitLab and wait for the CI/CD job to finish. The multidev creation takes a few minutes, so be patient.

When it is finished, go check out the multidev list on Pantheon. The `mr-2` multidev should be there.

*More CI/CD options through [Terminus Build Tools](https://github.com/pantheon-systems/terminus-build-tools-plugin/).*
>>>
